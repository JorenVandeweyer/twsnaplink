/*
 * (C) Copyright Joren Vandeweyer 2017
 *
 * @Author:        Joren Vandeweyer
 * @Email:         jorenvandeweyer@gmail.com
 * @Date:          2017-06-19 15:57:16
 * @Last Modified: 2017-06-19 16:25:03
 */

// ==UserScript==
// @name         TWSnapLink
// @version      1
// @description  SnapLink for TW
// @match        https://*.tribalwars.nl/*
// @grant        GM_openInTab
// @updateURL    https://bitbucket.org/JorenVandeweyer/twsnaplink/raw/master/TWSnapLink.user.js
// @downloadURL  https://bitbucket.org/JorenVandeweyer/twsnaplink/raw/master/TWSnapLink.user.js
// ==/UserScript==
 
(function() {

	var ox, oy;
	var selection = false;
	var selectedUrls = [];
	var urls = [];

	function openSelection() {
		var popup = document.createElement("div");
		popup.id = "TWSnapLink";

		popup.style.cssText = "background-color:transparent;border:1px solid #000000;top:0px;left:0px;position:absolute;width:0px;height:0px";
		$("body").append(popup);
	}

	function createSelected(left, top, width, height){
		var active = document.createElement("div");
		active.class = "TWSnapLinkClass";

		active.style.cssText = "background-color:transparent;border:1px solid red;top:" + top + "px;left:" + left + "px;width:" + width + "px;height:" + height + "px;position:absolute";
		document.getElementById("TWSnapLink").append(active);
		return active;
	}

	function updateSelection(x, y){
		var left, top, width, height;

		if(x < ox){
			left = x;
		} else {
			left = ox;
		}
		if(y < oy){
			top = y;
		} else {
			top = oy;
		}

		var obj = document.getElementById("TWSnapLink");
		obj.style.left = left + "px";
		obj.style.top = top + "px";
		obj.style.width = Math.abs(x - ox) + "px";
		obj.style.height = Math.abs(y - oy) + "px";
	}

	function getBoxObjectForXY(element) {
		var x = 0;
		var y = 0;
		var parent = element;
		while (parent) {
			x += parent.offsetLeft;
			y += parent.offsetTop;
			parent = parent.offsetParent;
		}

		parent = element;
		while (parent && parent != document.body && parent != document.documentElement) {
			if (parent.scrollLeft){
				x -= parent.scrollLeft;
			}
			if (parent.scrollTop){
				y -= parent.scrollTop;
			}
			parent = parent.parentNode;
		}

		return {
			x: x,
			y: y
		};
	}

	function getBoxObjectFor(obj) {
		var xy = getBoxObjectForXY(obj);
		return {
			x: xy.x,
			y: xy.y,
			width: obj.offsetWidth,
			height: obj.offsetHeight
		};
	}

	function isVisible(obj) {
		var parent = obj;
		while (parent) {
			if (jQuery(parent).css('display') == 'none'){
				return false;
			}
			if (jQuery(parent).css('visibility') == 'hidden'){
				return false;
			}
			if (jQuery(parent).css('z-index') < 0){
				return false;
			}
			parent = parent.offsetParent;
		}
		return true;
	}

	function openTab(){
		if(urls.length){
			if(urls[0]){
				GM_openInTab(urls.shift());
				setTimeout(function(){
					openTab(urls);
				}, 250);
			} else {
				urls.shift();
				openTab(urls);
			}
		}
	}

	document.onmousedown = function(e){
		if(e.button != 2) return;
		e.stopPropagation();
		e.preventDefault();
		if(!selection){
			selection = true;
			ox = e.pageX;
			oy = e.pageY;
			openSelection();
		} else {

			selection = false;
			e.stopPropagation();
			e.preventDefault();
			document.getElementById("TWSnapLink").remove();

			for (var k in urls){
				for (var j in urls){
					if (k != j && urls[k] == urls[j]){
						urls[j] = false;
					}
				}
			}

			// for (var k in urls){
			// 	if (urls[k]){
			// 		openTab(urls[k]);
			// 	}
			// }

			openTab();

			while(selectedUrls.length){
				selectedUrls.pop().remove();
			}

		}
	};

	document.onmousemove = function(e){
		if(selection){
			updateSelection(e.pageX, e.pageY);

			var tsize = getBoxObjectFor(document.getElementById("TWSnapLink"));

			while(selectedUrls.length){
				selectedUrls.pop().remove();
			}

			urls = [];

			for (i = 0; i < document.links.length; i++) {
				var link = document.links[i];
				lsize = getBoxObjectFor(link);

				if (link.href && link.href.indexOf('javascript:') >= 0){
					continue;
				}
				if (!lsize || lsize.y + lsize.height < tsize.y || tsize.y + tsize.height < lsize.y){
					continue;
				}
				if (!lsize || lsize.x + lsize.width < tsize.x || tsize.x + tsize.width < lsize.x){
					continue;
				}
				if (!isVisible(link)){
					continue;
				}

				var left = lsize.x - tsize.x - 2;
				var top = lsize.y - tsize.y - 2;
				var width = lsize.width;
				var height = lsize.height;

				selectedUrls.push(createSelected(left, top, width, height));

				urls.push(link.href);
			}

		}
	};

	document.oncontextmenu = function(e){
		e.preventDefault();
	};

})();
